import discord
import os
from sustain import sustain

client = discord.Client()
roles = ['DST.Community', 'DST.Status']

@client.event
async def on_ready():
  print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
  if message.author.bot: return

  if message.channel.name != 'dst-bot': return

  guild_roles = message.guild.roles  

  text = message.content
  args = text.split()
  arg = ''

  if len(args) == 2:
    arg = args[1]

  if text.startswith('.'):
    if text.find('help') == 1:
      await message.channel.send('`.add ROLE` - add you to role\n`.remove ROLE` - removes you from ROLE\n\nAvailable self-assignable roles:\nDST.Community\nDST.Status')
    elif text.find('add') == 1:
      if arg in roles:
        current_role = next((x for x in message.author.roles if str(x) == arg), None)
        
        if current_role != None:
          await message.channel.send('Can\'t add to ' + arg + ' - already a member of target role')
        else:
          role = next((x for x in guild_roles if str(x) == arg), None)
          if role != None:
            await message.author.add_roles(role)
            await message.channel.send('Added to role ' + arg)
      else:
        await message.channel.send('Role ' + arg + ' is not self-assignable')
    elif text.find('remove') == 1:
      if arg in roles:
        current_role = next((x for x in message.author.roles if str(x) == arg), None)

        if current_role != None:
          role = next((x for x in guild_roles if str(x) == arg), None)
          if role != None:
            await message.author.remove_roles(role)
            await message.channel.send('Removed from role ' + arg)
        else:
          await message.channel.send('You\'re not a member of role ' + arg)
      else:
        await message.channel.send('Role ' + arg + ' is not self-unassignable')

sustain()

client.run(os.getenv('TOKEN'))